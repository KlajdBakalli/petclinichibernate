package repository.impl;

import entities.UserEntity;
import entities.VeterinaryEntity;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.Repository;

import java.util.List;
@AllArgsConstructor
public class VeterinaryRepositoryImpl implements Repository<VeterinaryEntity,Integer> {
    private final Session session;
    @Override
    public VeterinaryEntity create(VeterinaryEntity veterinaryEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(veterinaryEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return veterinaryEntity;
    }


    @Override
    public List<VeterinaryEntity> readAll() {
        List<VeterinaryEntity> veterinary = (List<VeterinaryEntity>) this.session.createQuery("from VeterinaryEntity ").list();
        return veterinary;
    }

    @Override
    public VeterinaryEntity read(Integer veterinaryId) {
        VeterinaryEntity veterinaryEntity = session.find(VeterinaryEntity.class, veterinaryId);
        return veterinaryEntity;
    }

    @Override
    public VeterinaryEntity update(VeterinaryEntity veterinaryEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(veterinaryEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return veterinaryEntity;
    }

    @Override
    public void delete(Integer id) {

        VeterinaryEntity veterinary = read(id);
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            this.session.remove(veterinary);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
