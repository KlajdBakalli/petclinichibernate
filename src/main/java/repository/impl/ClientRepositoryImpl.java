package repository.impl;

import entities.ClientEntity;
import entities.UserEntity;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.Repository;

import java.util.List;

@AllArgsConstructor

public class ClientRepositoryImpl implements Repository<ClientEntity, Integer> {
    private final Session session;

    @Override
    public ClientEntity create(ClientEntity clientEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(clientEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return clientEntity;
    }

    @Override
    public List<ClientEntity> readAll() {
        List<ClientEntity> client = (List<ClientEntity>) this.session.createQuery("from ClientEntity ").list();
        return client;
    }

    @Override
    public ClientEntity read(Integer clientId) {
        ClientEntity clientEntity = session.find(ClientEntity.class, clientId);
        return clientEntity;
    }


    @Override
    public ClientEntity update(ClientEntity clientEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(clientEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return clientEntity;
    }

    @Override
    public void delete(Integer id) {
        ClientEntity client = read(id);
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            this.session.remove(client);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
