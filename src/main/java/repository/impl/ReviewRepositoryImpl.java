package repository.impl;

import entities.ReviewEntity;
import entities.UserEntity;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.Repository;

import java.util.List;
@AllArgsConstructor
public class ReviewRepositoryImpl implements Repository<ReviewEntity,Integer> {
    private final Session session;
    @Override
    public ReviewEntity create(ReviewEntity reviewEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(reviewEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return reviewEntity;
    }

    @Override
    public List<ReviewEntity> readAll() {
        List<ReviewEntity> review = (List<ReviewEntity>) this.session.createQuery("from ReviewEntity ").list();
        return review;
    }

    @Override
    public ReviewEntity read(Integer reviewId) {
       ReviewEntity reviewEntity = session.find(ReviewEntity.class, reviewId);
        return reviewEntity;
    }

    @Override
    public ReviewEntity update(ReviewEntity reviewEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(reviewEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return reviewEntity;
    }

    @Override
    public void delete(Integer id) {
        ReviewEntity review = read(id);
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            this.session.remove(review);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
