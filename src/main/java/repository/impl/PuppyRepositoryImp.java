package repository.impl;

import entities.PuppyEntity;
import entities.UserEntity;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.Repository;

import javax.persistence.NoResultException;
import java.util.List;
@AllArgsConstructor
public class PuppyRepositoryImp implements Repository<PuppyEntity,Integer> {
    private final Session session;
    @Override
    public PuppyEntity create(PuppyEntity puppyEntity) {

        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(puppyEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return puppyEntity;
    }

    @Override
    public List<PuppyEntity> readAll() {
        List<PuppyEntity> puppy = (List<PuppyEntity>) this.session.createQuery("from PuppyEntity ").list();
        return puppy;
    }

    @Override
    public PuppyEntity read(Integer puppyId) {
        PuppyEntity puppyEntity= session.find(PuppyEntity.class, puppyId);
        return puppyEntity;
    }

    @Override
    public PuppyEntity update(PuppyEntity puppyEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(puppyEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return puppyEntity;
    }

    @Override
    public void delete(Integer id) {
        PuppyEntity puppy = read(id);
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            this.session.remove(puppy);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public PuppyEntity findPuppyByName(String name){
        try{
            return (PuppyEntity) this.session.createQuery("from PuppyEntity p where p.name= :name")
                    .setParameter("name",name)
                    .getSingleResult();
        }catch (NoResultException e){
            return null;
        }

    }

    }

