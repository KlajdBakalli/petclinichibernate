package repository.impl;

import entities.ReservationEntity;
import entities.UserEntity;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.Repository;

import java.util.List;

@AllArgsConstructor
public class ReservationRepositoryImpl implements Repository<ReservationEntity, Integer> {
    private final Session session;

    @Override
    public ReservationEntity create(ReservationEntity reservationEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(reservationEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return reservationEntity;
    }

    @Override
    public List<ReservationEntity> readAll() {
        List<ReservationEntity> reservation = (List<ReservationEntity>) this.session.createQuery("from ReservationEntity ").list();
        return reservation;
    }

    @Override
    public ReservationEntity read(Integer reservationId) {
        ReservationEntity reservationEntity = session.find(ReservationEntity.class, reservationId);
        return reservationEntity;
    }

    @Override
    public ReservationEntity update(ReservationEntity reservationEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(reservationEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return reservationEntity;
    }

    @Override
    public void delete(Integer id) {


       ReservationEntity reservation = read(id);
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            this.session.remove(reservation);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
