package repository.impl;

import com.google.protobuf.TypeRegistry;
import entities.UserEntity;
import lombok.AllArgsConstructor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@AllArgsConstructor
public class UserRepositoryImpl implements Repository<UserEntity, Integer> {

    private final Session session;

    @Override
    public UserEntity create(UserEntity userEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(userEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return userEntity;
    }

    @Override
    public List<UserEntity> readAll() {
        List<UserEntity> users = (List<UserEntity>) this.session.createQuery("from UserEntity").list();
        return users;
    }

    @Override
    public UserEntity read(Integer userId) {

        UserEntity userEntity = session.find(UserEntity.class, userId);
        return userEntity;
    }

    @Override
    public UserEntity update(UserEntity userEntity) {
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(userEntity);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return userEntity;
    }

    @Override
    public void delete(Integer id) {

        UserEntity user = read(id);
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            this.session.remove(user);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public UserEntity login(String username, String pass) {
        try {

            return (UserEntity) this.session.createQuery("from UserEntity u where u.username= :username and u.password= :pass")
                    .setParameter("username", username)
                    .setParameter("pass", pass)
                    .getSingleResult();
        }catch (NoResultException e){
            return null;
        }
    }
}
