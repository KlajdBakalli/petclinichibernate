package service;

import config.HibernateConfig;
import entities.ClientEntity;
import entities.PuppyEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import repository.impl.PuppyRepositoryImp;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class PuppyService {

    public static SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
    public static Session session = sessionFactory.openSession();

    PuppyRepositoryImp puppyRepositoryImp = new PuppyRepositoryImp(session);


    public PuppyEntity addNewPuppy(String name, String breed, LocalDate birthday, boolean isVaccinated, ClientEntity client) {
        PuppyEntity puppy = puppyRepositoryImp.create(PuppyEntity.builder()
                .name(name)
                .breed(breed)
                .birthday(birthday)
                .isVaccinated(isVaccinated)
                .clientEntity(client)
                .build());
        return puppy;
    }

    public void puppyFound(String name) {
        PuppyEntity puppy = puppyRepositoryImp.findPuppyByName(name);
        if (puppy != null) {
            System.out.println("Puppy Found : " + puppy.getName());
        } else {
            System.out.println("Not found ! ");
        }
    }
}
