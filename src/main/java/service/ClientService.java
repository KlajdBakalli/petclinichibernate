package service;

import config.HibernateConfig;
import entities.ClientEntity;
import entities.UserEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import repository.impl.ClientRepositoryImpl;
import repository.impl.UserRepositoryImpl;

public class ClientService {
    public static SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
    public static Session session = sessionFactory.openSession();

    ClientRepositoryImpl clientRepository = new ClientRepositoryImpl(session);

    public ClientEntity addNewClient(String name, String surname, int phoneNumber, String email, UserEntity user) {
        ClientEntity client = clientRepository.create(ClientEntity.builder()
                .name(name)
                .surname(surname)
                .phoneNumber(phoneNumber)
                .email(email)
                .userEntity(user)
                .build());
        return client;
    }
}
