package service;

import config.HibernateConfig;
import entities.UserEntity;
import entities.VeterinaryEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import repository.impl.VeterinaryRepositoryImpl;


public class VeterinaryService {
    public static SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
    public static Session session = sessionFactory.openSession();

    VeterinaryRepositoryImpl veterinaryRepository = new VeterinaryRepositoryImpl(session);

    public VeterinaryEntity addNewVeterinary(String name, String surname, String adreess, String role, UserEntity user) {
        VeterinaryEntity veterinary = veterinaryRepository.create(VeterinaryEntity.builder()

                .name(name)
                .surname(surname)
                .address(adreess)
                .veterinaryRole(role)
                .userEntity(user)
                .build());
        return veterinary;
    }
}
