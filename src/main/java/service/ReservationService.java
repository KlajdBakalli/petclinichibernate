package service;

import config.HibernateConfig;
import entities.PuppyEntity;
import entities.ReservationEntity;
import entities.UserEntity;
import entities.VeterinaryEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import repository.impl.ReservationRepositoryImpl;


import java.time.LocalDate;
import java.time.LocalDateTime;

public class ReservationService {
    public static SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
    public static Session session = sessionFactory.openSession();

    ReservationRepositoryImpl reservationRepository = new ReservationRepositoryImpl(session);

    public ReservationEntity addNewReservation(LocalDateTime dateTime, PuppyEntity puppy, VeterinaryEntity veterinary) {
        ReservationEntity reservation = reservationRepository.create(ReservationEntity.builder()
                .reservationDate(dateTime)
                .puppyEntity(puppy)
                .veterinaryEntity(veterinary)
                .build());
        return reservation;
    }
}
