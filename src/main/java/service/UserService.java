package service;

import config.HibernateConfig;
import entities.UserEntity;
import lombok.Builder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import repository.impl.UserRepositoryImpl;

public class UserService {
    public static SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
    public static Session session = sessionFactory.openSession();

    UserRepositoryImpl userRepositoryImpl = new UserRepositoryImpl(session);

    public UserEntity addNewUser(String name, String password, boolean isAdmin, boolean isCLient, boolean isVeterinary) {
        UserEntity user = userRepositoryImpl.create(UserEntity.builder()
                .username(name)
                .password(password)
                .isAdmin(isAdmin)
                .isClient(isCLient)
                .isVeterinary(isVeterinary)
                .build());

        return user;
    }

    public void update(UserEntity user) {
        userRepositoryImpl.update(user);
    }

    public void readFrom(Integer id) {
        UserEntity user = userRepositoryImpl.read(id);
        System.out.println("U shtua " + user.getUserId());
    }


    public void delete(Integer id) {
        userRepositoryImpl.delete(id);
    }

    public void login(String username, String pass) {
        UserEntity user = userRepositoryImpl.login(username, pass);
        if (user != null) {
            System.out.println("User exists : "+user);
        } else {
            System.out.println(" User not found ");
        }
    }
}
