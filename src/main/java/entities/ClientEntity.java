package entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "client")
public class ClientEntity {

    @Column(name = "client_id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int clientId;
    @Column
    private String name;
    @Column
    private String surname;
    @Column
    private int phoneNumber;
    @Column
    private String email;

    @OneToOne
    @JoinColumn(name = "userId")
    private UserEntity userEntity;

    @OneToMany(mappedBy = "clientEntity")
    List<PuppyEntity> puppyEntities;

    @OneToOne(mappedBy = "clientEntity")
    private ReviewEntity reviewEntity;


}
