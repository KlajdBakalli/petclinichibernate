package entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "puppy")
public class PuppyEntity {
    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int puppyId;
    @Column
    private String name;
    @Column
    private String breed;
    @Column
    private LocalDate birthday;

    @Column(name ="isVaccinated" )

    private boolean isVaccinated;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private ClientEntity clientEntity;
    @OneToMany(mappedBy = "puppyEntity")
    private List<ReservationEntity> reservationEntities;

}
