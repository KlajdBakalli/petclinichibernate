package entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "veterinary")
public class VeterinaryEntity {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int veterinaryId;

    @Column
    private String name;
    @Column
    private String surname;
    @Column
    private String address;
    @Column(name = "veterinaryrole")
    private String veterinaryRole;

    @OneToOne
    @JoinColumn(name = "userId")
    private UserEntity userEntity;

    @OneToMany(mappedBy = "veterinaryEntity")
    private List<ReservationEntity> reservationEntityList;

}
