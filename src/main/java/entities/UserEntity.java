package entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user")
public class UserEntity {
    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private boolean isAdmin;
    @Column
    private boolean isClient;
    @Column
    private boolean isVeterinary;

    @OneToOne(mappedBy = "userEntity")
    private ClientEntity clientEntity;

    @OneToOne(mappedBy = "userEntity")
    private VeterinaryEntity veterinaryEntity;
}
