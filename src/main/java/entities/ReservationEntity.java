package entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "reservations")
public class ReservationEntity {
    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int reservationsId;

    @Column
    private LocalDateTime reservationDate;


            @ManyToOne
    @JoinColumn(name = "puppyId")
    private PuppyEntity puppyEntity;

            @ManyToOne
    @JoinColumn(name = "veterinaryId")
    private VeterinaryEntity veterinaryEntity;

}
