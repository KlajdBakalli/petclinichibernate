import config.HibernateConfig;
import entities.ClientEntity;
import entities.PuppyEntity;
import entities.UserEntity;
import entities.VeterinaryEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import repository.impl.UserRepositoryImpl;
import service.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Application {
    public static SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

    public static Session session = sessionFactory.openSession();

    public static void main(String[] args) {

        UserService userService = new UserService();
        UserEntity user = userService.addNewUser("Dhimo", "kaio", false, true, false);
        ClientService clientService = new ClientService();
        ClientEntity client = clientService.addNewClient("Dhimo", "Dhimo", 68555503, "dh_s@gmail", user);


        UserService userService1 = new UserService();
        UserEntity user1 = userService.addNewUser("Pajo", "251", false, false, true);
        VeterinaryService veterinaryService = new VeterinaryService();
        VeterinaryEntity veterinary = veterinaryService.addNewVeterinary("Pajo", "Hoxha", "Tirana", "Vaksinues", user1);

        PuppyService puppyService = new PuppyService();
        PuppyEntity puppy = puppyService.addNewPuppy("Laki", "Haski", LocalDate.of(1999, 05, 5), true, client);

        ReservationService reservationService = new ReservationService();
        reservationService.addNewReservation(LocalDateTime.of(2021, 02, 25, 10, 30), puppy, veterinary);

        userService.login("Pajo", "251");
        puppyService.puppyFound("Laki");
    }

}
